#pragma once
#include "common.h"
#include "comunicazione.h"
#include "protocol.h"
#include <semaphore.h>
// parametri che servono al thread che si occupa di gestire la comunicazione dei
// comandi al firmware
typedef struct {
  volatile int *runThread;
  sem_t *socket_sem;
  int socket_desc;
} LoggerParams;

// funzione eseguita dal thread
void *_loggerFn(void *args_);