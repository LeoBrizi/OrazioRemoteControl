#include "common.h"
#include "comunicazione.h"
#include "log_manager.h"
#include "protocol.h"
#include <pthread.h>
#include <semaphore.h>

#include <opencv2/core/core.hpp>
//#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>

volatile int running = 1;
volatile int faseSetup = 1;

sem_t *richiesta_socket;
pthread_t logger_thread;

// bisogna gestire il ctrl-c

// serve per waitKey()
// converte i comandi dal formato int al formato char*
// usa le macro di common.h
void input_converter(int input, char **output) {
  switch (input) {
  case 113: // q
    sprintf(*output, "%s", INCREASE_BRIGHTNESS);
    break;
  case 119: // w
    sprintf(*output, "%s", DECREASE_BRIGHTNESS);
    break;
  case 97: // a
    sprintf(*output, "%s", INCREASE_CONTRAST);
    break;
  case 115: // s
    sprintf(*output, "%s", DECREASE_CONTRAST);
    break;
  case 122: // z
    sprintf(*output, "%s", INCREASE_SATURATION);
    break;
  case 120: // x
    sprintf(*output, "%s", DECREASE_SATURATION);
    break;
  case 112: // p
    sprintf(*output, "%s", STOP);
    break;
  case 102: // f
    sprintf(*output, "%s", FOTO);
    break;
  case 27: // esc
    sprintf(*output, "%s", QUIT_COMMAND);
    break;
  default:
    sprintf(*output, "%s", "b");
  }
}

void sigintHandler(int sig) {
  printf("\n");
  printf("\033[0;31m");
  printf("ricevuto il segnale di terminazione...");
  printf("per terminare inviare QUIT al server ");
  printf("\033[0m");
  printf("\n");
}

void printHelp() {
  printf("per andare avanti usare il comando: 'w'\n");
  printf("per andare indietro usare il comando 's'\n");
  printf("per girare a destra usare il comando 'd'\n");
  printf("per girare a sinistra usare il comando 'a'\n");
  printf("per fermarsi usare il comando 'p'\n");
}

void printHelpSetup() {
  printf("per aumentare la luminosità usare il comando: 'q'\n");
  printf("per diminuire la luminosità usare il comando: 'w'\n");

  printf("per aumentare il contrasto usare il comando: 'a'\n");
  printf("per diminuire la contrasto usare il comando: 's'\n");

  printf("per aumentare la saturazione usare il comando: 'z'\n");
  printf("per diminuire la saturazione usare il comando: 'x'\n");

  printf("per scattare una foto di prova senza cambiare parametri usare il "
         "comando 'f'\n");
  printf("per uscire della fase di setup della webcam usare il comando 'p'\n");
}

void controlSession(int socket_desc) {
  printf("per terminare inserire QUIT\n");
  char *close_command = (char *)QUIT_COMMAND;
  size_t close_command_len = strlen(close_command);

  char *avanti = (char *)AVANTI;
  char *indietro = (char *)INDIETRO;
  char *sinistra = (char *)SINISTRA;
  char *destra = (char *)DESTRA;
  char *stop = (char *)STOP; // usata anche nella fase di setup webcam
  char *foto = (char *)FOTO; // usata anche nella fase di setup webcam
  // VARIABILI PER SETUP WEBCAM
  char *increase_brightness = (char *)INCREASE_BRIGHTNESS;
  char *decrease_brightness = (char *)DECREASE_BRIGHTNESS;
  char *increase_contrast = (char *)INCREASE_CONTRAST;
  char *decrease_contrast = (char *)DECREASE_CONTRAST;
  char *increase_saturation = (char *)INCREASE_SATURATION;
  char *decrease_saturation = (char *)DECREASE_SATURATION;

  char *socket_buffer =
      (char *)malloc(BUFFER_RECV_CLIENT); // buffer di ricezione dei pacchetti
  char *input_cmd =
      (char *)malloc(COMMAND_SIZE); // buffer per acquisire comando da tastiera

  PacketHeaderS *ack_packet;
  int packet_len;
  int ret = 0;

  // servono per visualizzare l'immagine tramite opencv:
  int prima_iterazione = 1;
  int key;
  cv::Mat image;
  cv::Mat im_loading;

  while (running) {
    // azzero le variabili
    packet_len = 0;
    memset(socket_buffer, 0, BUFFER_RECV_CLIENT);
    memset(input_cmd, 0, COMMAND_SIZE);

    printf("pronto per acquisire un comando: ");

    if (faseSetup) {
      if (prima_iterazione) {
        prima_iterazione = 0;

        // nella prima iterazione ancora non e' stata creata alcuna finestra con
        // opencv
        // quindi non si puo' chiamare waitKey()
        printHelpSetup();
        scanf("%s", input_cmd); // acquisire da tastira il comando
        // CREAZIONE FINESTRA PER VISUALIZZAZIONE IMMAGINE
        cv::namedWindow("Setup Webcam", cv::WINDOW_AUTOSIZE);
        im_loading = imread("loading.jpeg", cv::IMREAD_COLOR);
        cv::imshow("Setup Webcam", im_loading);
        cv::imshow("Setup Webcam", im_loading); // meglio chiamarla due volte
        cv::waitKey(1);
      } else {
        // in questo caso una finestra e' gia' stata creata ed e' attiva
        key = cv::waitKey();
        input_converter(key, &input_cmd);
      }
    } else {
      // in questo caso la finestra e' stata distrutta precedentemente
      scanf("%s", input_cmd); // acquisire da tastira il comando
    }
    if (strlen(input_cmd) == 0)
      continue;
    printf("\n");
    printf("il comando acquisito è: %s\n", input_cmd);

    if (strlen(input_cmd) == close_command_len &&
        !memcmp(input_cmd, close_command, close_command_len)) {
      running = 0;
      fprintf(stderr, "Sessione di controllo terminata\n");
      break;
    }

    if (faseSetup) {
      if (memcmp(input_cmd, increase_brightness, strlen(input_cmd)) &&
          memcmp(input_cmd, decrease_brightness, strlen(input_cmd)) &&
          memcmp(input_cmd, increase_contrast, strlen(input_cmd)) &&
          memcmp(input_cmd, decrease_contrast, strlen(input_cmd)) &&
          memcmp(input_cmd, increase_saturation, strlen(input_cmd)) &&
          memcmp(input_cmd, decrease_saturation, strlen(input_cmd)) &&
          memcmp(input_cmd, foto, strlen(input_cmd)) &&
          memcmp(input_cmd, stop, strlen(input_cmd))) {
        printf("\033[0;31m");
        printf("IL COMANDO INSERITO NON È VALIDO, UTILIZZARE:\n");
        printf("\033[0m");
        printHelpSetup();
        printf("\n");
        continue;
      }
    } else {
      if (memcmp(input_cmd, avanti, strlen(input_cmd)) &&
          memcmp(input_cmd, avanti, strlen(input_cmd)) &&
          memcmp(input_cmd, indietro, strlen(input_cmd)) &&
          memcmp(input_cmd, sinistra, strlen(input_cmd)) &&
          memcmp(input_cmd, destra, strlen(input_cmd)) &&
          memcmp(input_cmd, stop, strlen(input_cmd))) {
        printf("\033[0;31m");
        printf("IL COMANDO INSERITO NON È VALIDO, UTILIZZARE:\n");
        printf("\033[0m");
        printHelp();
        printf("\n");
        continue;
      }
    }

    CommandPacket *cmd_packet = (CommandPacket *)malloc(sizeof(CommandPacket));
    ((PacketHeaderS *)cmd_packet)->type = CommandMsg;
    cmd_packet->cmd = (char *)malloc(strlen(input_cmd) + 1);
    memcpy(cmd_packet->cmd, input_cmd, strlen(input_cmd) + 1);

    packet_len = Packet_serialize(socket_buffer, (PacketHeaderS *)cmd_packet);

    Packet_free((PacketHeaderS *)cmd_packet);

    printf("invio pacchetto comando...");

    // sessione critica
    ret = sem_wait(richiesta_socket);
    PRINT_ERROR((ret != 0), errno, "Errore nella sem_post richiesta_socket\n");

    ret = invia(socket_desc, socket_buffer, packet_len, (int *)&running);
    if (ret < 0) {
      running = 0;
      ret = sem_post(richiesta_socket);
      PRINT_ERROR((ret != 0), errno,
                  "Errore nella sem_post richiesta_socket\n");
      break;
    }

    printf(" comando inviato correttamente\n");

    printf("attesa dell'ack del server...");
    packet_len = ricevi(socket_desc, socket_buffer, (int *)&running);
    if (packet_len == -1) {
      running = 0;
      ret = -1;
      ret = sem_post(richiesta_socket);
      PRINT_ERROR((ret != 0), errno,
                  "Errore nella sem_post richiesta_socket\n");
      break;
    }

    // fine sessione critica
    ret = sem_post(richiesta_socket);
    PRINT_ERROR((ret != 0), errno, "Errore nella sem_post richiesta_socket\n");

    ack_packet = Packet_deserialize(socket_buffer, packet_len);
    if (ack_packet->type == AckMsg)
      printf("il server ha ricevuto il messaggio correttamente\n");
    else
      printf("Problema nella connessione con il server\n");
    Packet_free(ack_packet);

    if (faseSetup) {
      if (!memcmp(input_cmd, increase_brightness, strlen(input_cmd)) ||
          !memcmp(input_cmd, decrease_brightness, strlen(input_cmd)) ||
          !memcmp(input_cmd, increase_contrast, strlen(input_cmd)) ||
          !memcmp(input_cmd, decrease_contrast, strlen(input_cmd)) ||
          !memcmp(input_cmd, increase_saturation, strlen(input_cmd)) ||
          !memcmp(input_cmd, decrease_saturation, strlen(input_cmd)) ||
          !memcmp(input_cmd, foto, strlen(input_cmd))) {
        // devo ricevere la foto
        printf("attendo la ricezione dell'immagine...");
        char *socket_buffer_foto = (char *)malloc(BUFFER_RECV_FOTO_CLIENT);
        packet_len = ricevi(socket_desc, socket_buffer_foto, (int *)&running);
        if (packet_len == -1) {
          running = 0;
          ret = -1;
          free(socket_buffer_foto);
          break;
        }
        PacketHeaderS *picture =
            Packet_deserialize(socket_buffer_foto, packet_len);
        free(socket_buffer_foto);
        printf(" ricevuta\n");

        // VISUALIZZAZIONE IMMAGINE
        cv::Mat rawData(1, ((PicturePacket *)picture)->length, CV_8UC1,
                        (void *)((PicturePacket *)picture)->picture);
        image = cv::imdecode(rawData, cv::IMREAD_COLOR);
        if (!image.empty()) {
          cv::imshow("Setup Webcam", image);
          cv::waitKey(1);
        }

        Packet_free(picture);

      } else if (!memcmp(input_cmd, stop, strlen(input_cmd))) {

        // DISTRUZIONE DELLA FINESTRA PER L'IMMAGINE
        cv::destroyWindow("Setup Webcam");
        printHelp();
        faseSetup = 0;
        // avvio thread gestione log
        LoggerParams logger_params = {.runThread = &running,
                                      .socket_sem = richiesta_socket,
                                      .socket_desc = socket_desc};
        printf("fine fase di setup della webcam avvio thread per il log\n");
        pthread_attr_t logger_attr;
        pthread_attr_init(&logger_attr);
        ret = pthread_create(&logger_thread, &logger_attr, _loggerFn,
                             &logger_params);
        PRINT_ERROR((ret != 0), errno,
                    "errore creazione thread per la gestione del robot\n");
      }
    }
  }
  free(socket_buffer);
  free(input_cmd);
  if (faseSetup == 0) {
    void *runner_result;
    ret = pthread_join(logger_thread, &runner_result);
    PRINT_ERROR((ret != 0), errno,
                "errore nell'attesa thread per la gestione del file di log\n");
  }
  close(socket_desc);
}

int connectTo(in_addr_t ip_addr, uint16_t port_number) {
  int ret;
  int socket_desc;
  struct sockaddr_in server_addr = {0};

  // create socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  PRINT_ERROR((socket_desc < 0), errno, "Could not create socket\n");

  // set up parameters for the connection
  server_addr.sin_addr.s_addr = ip_addr;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port_number;

  // initiate a connection on the socket
  printf("in attesa di connessione al server\n");
  ret = connect(socket_desc, (struct sockaddr *)&server_addr,
                sizeof(struct sockaddr_in));
  PRINT_ERROR((ret < 0), errno, "Could not create connection\n");

  printf("connessione stabilita\n");

  return socket_desc;
}

int main(int argc, char *argv[]) {
  int ret;
  if (argc == 3) {
    // we use network byte order
    in_addr_t ip_addr;
    uint16_t port_number;

    // retrieve IP address
    ip_addr = inet_addr(argv[1]);

    // retrieve port number
    long tmp = strtol(argv[2], NULL, 0);
    if (tmp < 1024 || tmp > 49151) {
      fprintf(stderr, "Please use a port number between 1024 and 49151.\n");
      exit(EXIT_FAILURE);
    }
    port_number = htons((uint16_t)tmp);

    int socket_desc = connectTo(ip_addr, port_number);

    struct sigaction act = {0};

    act.sa_handler = sigintHandler;
    // voglio gestire il SIGINT perchè voglio chiudere tutto per bene
    ret = sigaction(SIGINT, &act, NULL);
    PRINT_ERROR((ret < 0), errno,
                "errore nell'instaurazione del gestore di SIGINT");

    richiesta_socket = (sem_t *)malloc(sizeof(sem_t));

    ret = sem_init(richiesta_socket, 0, 1);
    PRINT_ERROR((ret != 0), errno,
                "errore inizializzazione semaforo per l'attesa del socket");

    // start a chat session
    controlSession(socket_desc);

    ret = sem_destroy(richiesta_socket);
    PRINT_ERROR((ret != 0), errno, "Errore destroy richiesta_socket");
    free(richiesta_socket);
  } else {
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "%s <server_address> <port_number> \n", argv[0]);
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
}
