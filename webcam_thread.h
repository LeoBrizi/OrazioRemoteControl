#pragma once
#include "common.h"
#include "webcam_manager.h"
#include <semaphore.h>

typedef struct {
  volatile int *runThread;
  int *cmd;
  volatile int *fase_setup; // con 1 si e' in fase setup, con 0 si esce
  void **buffer;
  int *lunghezza;
  int *altezza;
  int *larghezza;
  sem_t *webcam_sem;
  sem_t *foto_pronta;
} WebcamParams;

void *_webcamFn(void *args_);
