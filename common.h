#ifndef __COMMON_H_
#define __COMMON_H_

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

// macro to simplify error handling
#define PRINT_ERROR(condizione, errCode, msg)                                  \
  do {                                                                         \
    if (condizione) {                                                          \
      printf("\n");                                                            \
      fprintf(stderr, "%s: %s\n", msg, strerror(errCode));                     \
      exit(EXIT_FAILURE);                                                      \
    }                                                                          \
  } while (0)

// queste define non dovrebbero servire
/*
#define CMD_FORWARD 01
#define CMD_BACK	02
#define CMD_LEFT	03
#define CMD_RIGHT	04
#define CMD_STOP	05
#define CMD_ODOM	06
#define CMD_PICTURE	07
*/

#define QUIT_COMMAND "QUIT"
#define AVANTI "w"
#define INDIETRO "s"
#define DESTRA "d"
#define SINISTRA "a"
#define FOTO "f" // usata anche in fase setup webcam
#define STOP "p" // usata anche in fase setup webcam
// MACRO PER FASE SETUP WEBCAM
#define INCREASE_BRIGHTNESS "q"
#define DECREASE_BRIGHTNESS "w"
#define INCREASE_CONTRAST "a"
#define DECREASE_CONTRAST "s"
#define INCREASE_SATURATION "z"
#define DECREASE_SATURATION "x"
// FINE MACRO PER FASE SETUP WEBCAM
#define COMMAND_SIZE 64
#define S_BUFFER_SIZE 64
#define SERVER_BUFFER 64
#define BUFFER_RECV_CLIENT 64
#define BUFFER_RECV_FOTO_CLIENT 1024000
#endif
