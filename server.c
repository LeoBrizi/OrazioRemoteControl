#include "common.h"
#include "comunicazione.h"
#include "orazio_client.h"
#include "orazio_thread.h"
#include "protocol.h"
#include "webcam_thread.h"
#include <pthread.h>
#include <semaphore.h>

volatile int running = 1;
volatile int run = 1;

int orazio_shared_mem;
float odom_x;
float odom_y;
float odom_theta;
int webcam_shared_mem;
volatile int fase_setup = 1;  // si inizia sempre con la fase setup
int imm_length, imm_h, imm_w; // vengono riempiti dal thread gestore della
                              // webcam e poi inviati al client
void *webcam_buffer;
sem_t *webcam_sem;
sem_t *foto_pronta;
sem_t *odom_sem;

void sigintHandler(int sig) {
  run = 0;
  running = 0;
  int ret;
  // sblocchiamo il thread in attesa
  ret = sem_post(webcam_sem);
  PRINT_ERROR((ret != 0), errno, "Errore nella sem_post webcam_sem\n");
  printf("ricevuto il segnale di terminazione...\n");
}

void controlSession(int socket_desc) {
  printf("avvio di una sessione di controllo:\n");

  char *close_command = (char *)QUIT_COMMAND;
  size_t close_command_len = strlen(close_command);

  char *buffer = (char *)malloc(SERVER_BUFFER);
  PacketHeaderS *recPacket;
  int packet_len;

  PacketHeaderS *ack_packet = (PacketHeaderS *)malloc(sizeof(PacketHeaderS));
  ack_packet->type = AckMsg;

  char *cmd;
  int ret;

  char *avanti = (char *)AVANTI;
  char *indietro = (char *)INDIETRO;
  char *sinistra = (char *)SINISTRA;
  char *destra = (char *)DESTRA;
  char *stop = (char *)STOP; // usata anche nella fase di setup webcam
  char *foto = (char *)FOTO; // usata anche nella fase di setup webcam
  // VARIABILI PER SETUP WEBCAM
  char *increase_brightness = (char *)INCREASE_BRIGHTNESS;
  char *decrease_brightness = (char *)DECREASE_BRIGHTNESS;
  char *increase_contrast = (char *)INCREASE_CONTRAST;
  char *decrease_contrast = (char *)DECREASE_CONTRAST;
  char *increase_saturation = (char *)INCREASE_SATURATION;
  char *decrease_saturation = (char *)DECREASE_SATURATION;

  while (running) {
    // aspetto un comando ed eseguo in sostanza
    // l'esecuzione dei comandi è sostanzialmente è sequenziale, si aspetta la
    // terminazione di un comando, si manda un ack e si passa al successivo
    packet_len = 0;
    memset(buffer, 0, SERVER_BUFFER);

    printf("in attesa di comandi dal client\n");
    packet_len = ricevi(socket_desc, buffer, (int *)&running);
    if (packet_len == -1) {
      ret = -1;
      running = 0;
      break;
    }
    if (packet_len == 0)
      continue;

    printf("pacchetto ricevuto\n");

    recPacket = Packet_deserialize(buffer, packet_len);
    cmd = (char *)malloc(strlen(((CommandPacket *)recPacket)->cmd) + 1);
    memcpy(cmd, ((CommandPacket *)recPacket)->cmd,
           strlen(((CommandPacket *)recPacket)->cmd) + 1);
    Packet_free(recPacket);

    packet_len = 0;
    memset(buffer, 0, SERVER_BUFFER);
    packet_len = Packet_serialize(buffer, ack_packet);
    printf("invio pacchetto ack...");
    ret = invia(socket_desc, buffer, packet_len, (int *)&running);
    if (ret < 0)
      running = 0;
    printf(" ack inviato\n");

    printf("comando ricevuto: %s \n", cmd);

    if (strlen(cmd) == close_command_len &&
        !memcmp(cmd, close_command, close_command_len)) {
      running = 0;
      fprintf(stderr, "Sessione di controllo terminata\n");
      free(cmd);
      continue; // prima rilascia la memoria dinamica, poi torna al test
                // condizionale del while
    }
    // FASE SETUP WEBCAM

    if (fase_setup == 1) {
      printf("fase setup webcam\n");
      // SETTAGGIO PARAMETRI WEBCAM
      if (!memcmp(cmd, increase_brightness, strlen(cmd)))
        webcam_shared_mem = 1;
      else if (!memcmp(cmd, decrease_brightness, strlen(cmd)))
        webcam_shared_mem = 2;
      else if (!memcmp(cmd, increase_contrast, strlen(cmd)))
        webcam_shared_mem = 3;
      else if (!memcmp(cmd, decrease_contrast, strlen(cmd)))
        webcam_shared_mem = 4;
      else if (!memcmp(cmd, increase_saturation, strlen(cmd)))
        webcam_shared_mem = 5;
      else if (!memcmp(cmd, decrease_saturation, strlen(cmd)))
        webcam_shared_mem = 6;
      else if (!memcmp(cmd, stop, strlen(cmd))) { // stop dice di uscire dalla
                                                  // fase di setup webcam
        fase_setup = 0; // in questo modo esce dalla fase setup
        // ATTENZIONE: fase_setup è condivisa con il thread quindi in questo
        // modo anche il thread esce dalla fase setup
        printf("fine fase setup webcam \n");
        continue;
      } else if (!memcmp(cmd, foto, strlen(cmd)))
        webcam_shared_mem = 7; // serve per far scattare una foto all'inizio
                               // senza che modifichi i parametri
      else {
        free(cmd);
        continue;
      }

      // sblocchiamo il thread in attesa
      ret = sem_post(webcam_sem);
      PRINT_ERROR((ret != 0), errno, "Errore nella sem_post webcam_sem\n");

      // attesa che la foto sia pronta nel buffer
      ret = sem_wait(foto_pronta);
      PRINT_ERROR((ret != 0), errno, "Errore nella sem_post foto_pronta\n");
      printf("foto scattata\n");

      // INVIO DELL'IMMAGINE AL CLIENT
      // ora l'immagine e' pronta per essere mandata
      printf("creazione pacchetto in setup webcam \n");

      PicturePacket *picture_packet =
          (PicturePacket *)malloc(sizeof(PicturePacket));
      picture_packet->header.type = PictureMsg;
      // bisogna dire che il buffer che contiene l'immagine e' mmappato nella
      // webcam quindi non va fatta la free (serve a Packet_free())
      picture_packet->mmapped_buffer = 1;
      picture_packet->length = imm_length;
      picture_packet->height = imm_h;
      picture_packet->width = imm_w;
      picture_packet->picture = webcam_buffer; // vuole un void*
      printf("pacchetto creato in setup webcam \n");
      printf("serializzazione pacchetto in setup webcam \n");

      char *buffer_imm = (char *)malloc(sizeof(PicturePacket) + imm_length);

      packet_len =
          Packet_serialize(buffer_imm, (PacketHeaderS *)picture_packet);
      Packet_free((PacketHeaderS *)picture_packet);
      printf("%d\n", packet_len);
      printf("invio pacchetto immagine...");
      ret = invia(socket_desc, buffer_imm, packet_len, (int *)&running);
      if (ret < 0)
        running = 0;
      printf(" immagine inviata\n");
      free(buffer_imm);
      // FINE FASE SETUP WEBCAM
    } else {
      // FASE ESECUZIONE COMANDI
      printf("inizio fase di controllo robot\n");
      if (!memcmp(cmd, avanti, strlen(cmd)))
        orazio_shared_mem = 1;
      else if (!memcmp(cmd, indietro, strlen(cmd)))
        orazio_shared_mem = 2;
      else if (!memcmp(cmd, destra, strlen(cmd)))
        orazio_shared_mem = 3;
      else if (!memcmp(cmd, sinistra, strlen(cmd)))
        orazio_shared_mem = 4;
      else if (!memcmp(cmd, stop, strlen(cmd)))
        orazio_shared_mem = 5;
      else if (!memcmp(cmd, foto, strlen(cmd))) {
        ret = sem_post(webcam_sem);
        PRINT_ERROR((ret != 0), errno, "Errore nella sem_post webcam_sem\n");

        // attesa che la foto sia pronta nel buffer
        ret = sem_wait(foto_pronta);
        PRINT_ERROR((ret != 0), errno, "Errore nella sem_wait foto_pronta\n");

        // INVIO DELL'IMMAGINE
        printf("[NON FASE SETUP] foto richiesta dal client\n");
        PicturePacket *picture_packet =
            (PicturePacket *)malloc(sizeof(PicturePacket));
        picture_packet->header.type = PictureMsg;
        // bisogna dire che il buffer che contiene l'immagine e' mmappato nella
        // webcam quindi non va fatta la free (serve a Packet_free())
        picture_packet->mmapped_buffer = 1;
        picture_packet->length = imm_length;
        picture_packet->height = imm_h;
        picture_packet->width = imm_w;
        picture_packet->picture = webcam_buffer; // vuole un void*

        printf("serializzazione pacchetto\n");
        char *buffer_imm = (char *)malloc(sizeof(PicturePacket) + imm_length);
        packet_len =
            Packet_serialize(buffer_imm, (PacketHeaderS *)picture_packet);

        Packet_free(
            (PacketHeaderS *)picture_packet); // free del pacchetto serializzato

        printf("invio pacchetto immagine...");
        ret = invia(socket_desc, buffer_imm, packet_len,
                    (int *)&running); // la free(buffer) viene eseguita alla
                                      // fine del corpo del while
        if (ret < 0)
          running = 0;
        printf(" inviata\n");
        free(buffer_imm);

        // ACQUISIZIONE ODOMETRIA
        orazio_shared_mem = 6;
        ret = sem_wait(odom_sem);
        PRINT_ERROR((ret != 0), errno, "Errore nella sem_wait odom_pronta\n");

        OdomPacket *odometria = (OdomPacket *)malloc(sizeof(OdomPacket));
        odometria->header.type = OdomMsg;
        odometria->x = odom_x;
        odometria->y = odom_y;
        odometria->theta = odom_theta;

        memset(buffer, 0, SERVER_BUFFER);
        packet_len = Packet_serialize(buffer, (PacketHeaderS *)odometria);
        printf("odometria lunghezza: %d\n", ((PacketHeaderS *)buffer)->size);
        Packet_free((PacketHeaderS *)odometria);

        printf("invio pacchetto odometria...");
        ret = invia(socket_desc, buffer, packet_len, (int *)&running);
        if (ret < 0)
          running = 0;
        printf(" inviata\n");

      } else {
        free(cmd);
        continue;
      }
    }
    free(cmd);
  }

  orazio_shared_mem = 0;
  free(buffer);
  Packet_free(ack_packet);
  running = 1;
  fase_setup = 1;
  close(socket_desc);
  printf("La connesione è stata terminata\n");
}

void listenOnPort(uint16_t port_number) {
  int ret;
  int server_desc, client_desc;

  struct sockaddr_in server_addr = {0}, client_addr = {0};
  socklen_t sockaddr_len = sizeof(struct sockaddr_in);

  // initialize socket for listening
  server_desc = socket(AF_INET, SOCK_STREAM, 0);
  PRINT_ERROR((server_desc < 0), errno, "errore nella creazione del socket\n");

  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port_number;

  // We enable SO_REUSEADDR to quickly restart our server beacause the unbind
  // require time and we don't have time :)
  int reuseaddr_opt = 1;
  ret = setsockopt(server_desc, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt,
                   sizeof(reuseaddr_opt));
  PRINT_ERROR((ret < 0), errno, "error to setting SO_REUSEADDR option\n");

  // bind address to socket
  ret = bind(server_desc, (struct sockaddr *)&server_addr, sockaddr_len);
  PRINT_ERROR((ret < 0), errno, "errore nella bind address al socket\n");

  // start listening
  ret = listen(server_desc, 0); // non voglio nessuno in coda se arriva una
                                // connesione mentre la gestisco un'altra
                                // (ECONNREFUSED errore ritornato al client)
  PRINT_ERROR((ret < 0), errno, "Cannot listen on socket\n");

  // siccome la accept è una sistem call bloccante se mi arriva un interrupt
  // fallisce

  while (run) {
    printf("mi metto in attesa di una connessione\n");
    client_desc = accept(server_desc, (struct sockaddr *)&client_addr,
                         (socklen_t *)&sockaddr_len);
    if (client_desc == -1 && errno == EINTR)
      continue; //è arrivata un interruzione la syscall ha falllito la rifaccio

    printf("connessione accetta\n");
    // far partire il processo per lo stream
    controlSession(client_desc);
  }

  printf("chiusura in corso...\n");
  ret = close(server_desc);
  PRINT_ERROR((ret < 0), errno, "Cannot close listening socket\n");

  return;
}

int main(int argc, char *argv[]) {
  struct sigaction act = {0};

  act.sa_handler = sigintHandler;
  // voglio gestire il SIGINT perchè voglio chiudere tutto per bene
  int ret = sigaction(SIGINT, &act, NULL);
  PRINT_ERROR((ret < 0), errno,
              "errore nell'instaurazione del gestore di SIGINT");

  if (argc >= 3) {
    const char *device = argv[2];
    uint32_t baudrate = 115200;
    if (argc > 3)
      baudrate = atoi(argv[3]);
    printf("starting client on device [%s], with baudrate %d... ", device,
           baudrate);

    struct OrazioClient *client = OrazioClient_init(device, baudrate);
    if (!client) {
      printf("Failed\n");
      exit(EXIT_FAILURE);
    }
    printf("Success\n");
    printf("Synching...");
    OrazioClient_sync(client, 10);
    printf(" Done\n");

    OrazioClient_readConfiguration(client, 100);

    odom_sem = (sem_t *)malloc(sizeof(sem_t));

    ret = sem_init(odom_sem, 0, 0);
    PRINT_ERROR(
        (ret != 0), errno,
        "errore inizializzazione semaforo per l'attesa della odometria");

    OrazioParams orazio_params = {.client = client,
                                  .runThread = &run,
                                  .cmd = &orazio_shared_mem,
                                  .odom_x = &odom_x,
                                  .odom_y = &odom_y,
                                  .odom_theta = &odom_theta,
                                  .odom_sem = odom_sem};
    // avvio thread per l'esecuzione dei comandi
    pthread_attr_t orazio_attr;
    pthread_attr_init(&orazio_attr);
    pthread_t orazio_thread;
    ret =
        pthread_create(&orazio_thread, &orazio_attr, _orazioFn, &orazio_params);
    PRINT_ERROR((ret != 0), errno,
                "errore creazione thread per la gestione del robot\n");

    // AVVIO THREAD PER LA GESTIONE DELLE IMMAGINI
    // il semaforo serve per sincronizzare il thread gestore della webcam
    // ogni volta che viene chiamato, il server fa sem_post
    // il thread stesso esegue sem_wait ogni volta, prima di cominciare
    // il sem e' inizializzato a 0 perche' se non viene eseguita almeno una
    // sem_post bisogna aspettare

    foto_pronta = (sem_t *)malloc(sizeof(sem_t));
    webcam_sem = (sem_t *)malloc(sizeof(sem_t));

    ret = sem_init(foto_pronta, 0, 0);
    PRINT_ERROR((ret != 0), errno,
                "errore inizializzazione semaforo per l'attesa della webcam");

    // stessa cosa di foto_pronta
    ret = sem_init(webcam_sem, 0, 0);
    PRINT_ERROR(
        (ret != 0), errno,
        "errore inizializzazione semaforo per la gestione della webcam");

    WebcamParams webcam_params = {
        .runThread = &run,
        .cmd = &webcam_shared_mem,
        .fase_setup = &fase_setup,
        .buffer = &webcam_buffer, // attenzione! sto passando un **void
        .lunghezza = &imm_length,
        .altezza = &imm_h,
        .larghezza = &imm_w,
        .webcam_sem = webcam_sem,  // semaforo
        .foto_pronta = foto_pronta // semaforo
    };

    pthread_attr_t webcam_attr;
    pthread_attr_init(&webcam_attr);
    pthread_t webcam_thread;
    ret =
        pthread_create(&webcam_thread, &webcam_attr, _webcamFn, &webcam_params);
    PRINT_ERROR((ret != 0), errno,
                "errore creazione thread per la gestione della webcam");

    uint16_t port_number;
    long tmp = strtol(argv[1], NULL, 10);
    if (tmp < 1024 || tmp > 49151) {
      fprintf(stderr, "Please use a port number between 1024 and 49151.\n");
      exit(EXIT_FAILURE);
    }
    port_number = htons((uint16_t)tmp);

    listenOnPort(port_number);

    void *runner_result;
    ret = pthread_join(orazio_thread, &runner_result);
    PRINT_ERROR((ret != 0), errno,
                "errore nell'attesa thread per la gestione del robot\n");
    OrazioClient_destroy(client);

    ret = pthread_join(webcam_thread, &runner_result);
    PRINT_ERROR((ret != 0), errno,
                "errore nell'attesa thread per la gestione della webcam\n");

    // chiusura semafori e rilascio risore allocate dinamicamente
    ret = sem_destroy(foto_pronta);
    PRINT_ERROR((ret != 0), errno, "Errore destroy foto_pronta");
    ret = sem_destroy(webcam_sem);
    PRINT_ERROR((ret != 0), errno, "Errore destroy webcam_sem");
    ret = sem_destroy(odom_sem);
    PRINT_ERROR((ret != 0), errno, "Errore destroy odom_sem");
    free(foto_pronta);
    free(webcam_sem);
    free(odom_sem);
  } else {
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "%s <port_number> <device> [baudrate]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
}
