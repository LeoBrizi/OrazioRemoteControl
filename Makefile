PREFIX=.
CC = g++
INCLUDE_DIRS=-I$(PREFIX)/Orazio/ -I$(PREFIX)/
CFLAGS = -g -ggdb -fpermissive $(INCLUDE_DIRS)
CPPFLAGS = `pkg-config --cflags --libs opencv` -std=c++11
LDFLAGS = -lpthread

OBJS = comunicazione.o\
	orazio_thread.o\
	protocol.o\
	log_manager.o\
	webcam_manager.o\
	webcam_thread.o\
	orazio_client.o\
	packet_handler.o\
	serial_linux.o


HEADERS = common.h\
	comunicazione.h\
	orazio_thread.h\
	protocol.h\
	log_manager.h\
	webcam_manager.h\
	webcam_thread.h\
	buffer_utils.h\
	orazio_client.h\
	orazio_packets.h\
	packet_handler.h\
	packet_header.h\
	packet_operations.h\
	serial_linux.h

BINS = server\
	client

.phony: clean all

all: $(BINS)

%.o:	$(PREFIX)/Orazio/%.c 
	$(CC) $(CFLAGS) -c -o $@ $< $(CPPFLAGS)

%.o:	$(PREFIX)/%.c 
	$(CC) $(CFLAGS) -c -o $@ $< $(CPPFLAGS)

server: server.c $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(CPPFLAGS)

client:	 client.c $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(CPPFLAGS)

clean:
	rm -rf $(OBJS) $(BINS) *~ 

webcam_test:
	$(CC) tester_webcam_manager.c webcam_manager.c -o tester_webcam
