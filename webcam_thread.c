#include "webcam_thread.h"

void *_webcamFn(void *args_) {
  WebcamParams *args = (WebcamParams *)args_;
  int ret;
  int fd;
  int b_before, b_after;

  // FASE INIZIALIZZAZIONE WEBCAM

  fd = open("/dev/video0", O_RDWR);
  PRINT_ERROR((fd < 0), errno, "Errore apertura webcam \n");

  ret = initialize_webcam(fd, args->buffer, args->larghezza,
                          args->altezza); // initialize_webcam vuole un void**
  PRINT_ERROR((ret != 0), errno, "Errore inizialize_webcam \n");

  printf("\n RESET DI TUTTI I PARAMETRI \n\n");

  set_default_brightness(fd);

  set_default_contrast(fd);

  set_default_saturation(fd);

  while (*(args->runThread)) {
    // la sem_wait va prima del controllo fase_setup

    // attende che il server gli dica di partire
    ret = sem_wait(args->webcam_sem);
    PRINT_ERROR((ret != 0), errno, "Errore sem_wait di webcam_sem\n");

    // FASE SETUP PARAMETRI WEBCAM
    if (*(args->fase_setup) == 1) {
      switch (*(args->cmd)) {
      case 1:
        ret = change_brightness(fd, 10, &b_before, &b_after);
        PRINT_ERROR((ret != 0 && ret != 1), errno,
                    "Errore change_brightness \n");
        break;
      case 2:
        ret = change_brightness(fd, -10, &b_before, &b_after);
        PRINT_ERROR((ret != 0 && ret != 1), errno,
                    "Errore change_brightness \n");
        break;
      case 3:
        ret = change_contrast(fd, 10, &b_before, &b_after);
        PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_contrast \n");
        break;
      case 4:
        ret = change_contrast(fd, -10, &b_before, &b_after);
        PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_contrast \n");
        break;
      case 5:
        ret = change_saturation(fd, 10, &b_before, &b_after);
        PRINT_ERROR((ret != 0 && ret != 1), errno,
                    "Errore change_saturation \n");
        break;
      case 6:
        ret = change_saturation(fd, -10, &b_before, &b_after);
        PRINT_ERROR((ret != 0 && ret != 1), errno,
                    "Errore change_saturation \n");
        break;
      case 7:
        break; // in questo caso viene detto di scattare una foto senza
               // modificare i parametri
      default:
        continue;
      }

      ret = take_a_frame(fd, args->lunghezza);
      PRINT_ERROR((ret != 0), errno, "Errore take_a_frame \n");

      // dice al server che la foto e' pronta
      ret = sem_post(args->foto_pronta);
      PRINT_ERROR((ret != 0), errno, "Errore nella sem_post foto_pronta\n");
    } else {
      ret = take_a_frame(fd, args->lunghezza);
      PRINT_ERROR((ret != 0), errno, "Errore take_a_frame \n");

      // dice al server che la foto e' pronta
      ret = sem_post(args->foto_pronta);
      PRINT_ERROR((ret != 0), errno, "Errore nella sem_post foto_pronta\n");
    }
  }
  close(fd);
  return 0;
}
