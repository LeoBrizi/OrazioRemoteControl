#include "protocol.h"

// serializza un pacchetto in un buffer preallocato
int Packet_serialize(char *dest, PacketHeaderS *h) {
  char *dest_end = dest;
  switch (h->type) {
  case PictureMsg: {
    const PicturePacket *pic_packet = (PicturePacket *)h;
    memcpy(dest, pic_packet, sizeof(PicturePacket));
    dest_end += sizeof(PicturePacket);
    memcpy(dest_end, pic_packet->picture, pic_packet->length);
    dest_end += pic_packet->length;
    break;
  }
  case CommandMsg: {
    CommandPacket *cmd_packet = (CommandPacket *)h;
    memcpy(dest, cmd_packet, sizeof(CommandPacket));
    dest_end += sizeof(CommandPacket);
    memcpy(dest_end, cmd_packet->cmd, strlen(cmd_packet->cmd) + 1);
    dest_end += strlen(cmd_packet->cmd) + 1;
    break;
  }
  case OdomMsg: {
    OdomPacket *odom_packet = (OdomPacket *)h;
    memcpy(dest, odom_packet, sizeof(OdomPacket));
    dest_end += sizeof(OdomPacket);
    break;
  }
  case AckMsg: {
    memcpy(dest, h, sizeof(PacketHeaderS));
    dest_end += sizeof(PacketHeaderS);
    break;
  }
  }
  PacketHeaderS *dest_header = (PacketHeaderS *)dest;
  dest_header->size = dest_end - dest;
  return dest_header->size;
}

// returns a newly allocated packet read from the buffer
PacketHeaderS *Packet_deserialize(const char *buffer, int size) {
  const PacketHeaderS *h = (PacketHeaderS *)buffer;
  switch (h->type) {
  case PictureMsg: {
    PicturePacket *pic_packet = (PicturePacket *)malloc(sizeof(PicturePacket));
    memcpy(pic_packet, buffer, sizeof(PicturePacket));
    const void *picture = buffer + sizeof(PicturePacket);
    pic_packet->picture = malloc(pic_packet->length);
    memcpy(pic_packet->picture, picture, pic_packet->length);
    pic_packet->mmapped_buffer = 0; // vedi note in protocol.h
    return (PacketHeaderS *)pic_packet;
  }
  case CommandMsg: {
    CommandPacket *command_packet =
        (CommandPacket *)malloc(sizeof(CommandPacket));
    memcpy(command_packet, buffer, sizeof(CommandPacket));
    const char *comando = buffer + sizeof(CommandPacket);
    command_packet->cmd = (char *)malloc(strlen(comando) + 1);
    memcpy(command_packet->cmd, comando, strlen(comando) + 1);
    return (PacketHeaderS *)command_packet;
  }
  case OdomMsg: {
    OdomPacket *odom_packet = (OdomPacket *)malloc(sizeof(OdomPacket));
    memcpy(odom_packet, buffer, sizeof(OdomPacket));
    return (PacketHeaderS *)odom_packet;
  }
  case AckMsg: {
    PacketHeaderS *ack_packet = (PacketHeaderS *)malloc(sizeof(PacketHeaderS));
    memcpy(ack_packet, buffer, sizeof(PacketHeaderS));
    return ack_packet;
  }
  }
  return 0;
}

// deletes a packet, freeing memory
void Packet_free(PacketHeaderS *h) {
  switch (h->type) {
  case PictureMsg: {
    PicturePacket *pic_packet = (PicturePacket *)h;
    if (pic_packet->mmapped_buffer == 0)
      free(pic_packet->picture); // vedi note in protocol.h
    free(pic_packet);
    return;
  }
  case CommandMsg: {
    CommandPacket *cmd_packet = (CommandPacket *)h;
    free(cmd_packet->cmd);
    free(cmd_packet);
    return;
  }
  case OdomMsg: {
    free(h);
    return;
  }
  case AckMsg: {
    free(h);
    return;
  }
  }
}
