#include "comunicazione.h"

int invia(int fd, char *buffer, int msg_len, int *running) {
  int ret;
  int bytes_sent = 0;
  while (bytes_sent < msg_len && *running == 1) {
    ret = send(fd, buffer + bytes_sent, msg_len - bytes_sent, 0);
    if (ret == -1 && errno == EINTR)
      continue;
    if (ret < 0 && errno == ECONNRESET)
      return -1;
    bytes_sent += ret;
  }
  return bytes_sent;
}

int ricevi(int fd, char *buffer, int *running) {
  int ret;
  int bytes_read = 0;
  int header_size = sizeof(PacketHeaderS);
  while (bytes_read < header_size && *running == 1) {
    ret = recv(fd, buffer + bytes_read, header_size - bytes_read, 0);

    if (ret == -1 && errno == EINTR)
      continue;
    if (ret <= 0) {
      fprintf(stderr, "[WARNING] Endpoint closed the connection unexpectedly. "
                      "Exiting...\n");
      return -1; // connesione chiusa
    }
    bytes_read += ret;
  }

  PacketHeaderS *packet = (PacketHeaderS *)buffer;
  int bytes_to_read = packet->size;
  while (bytes_read < bytes_to_read && *running == 1) {
    ret = recv(fd, buffer + bytes_read, bytes_to_read - bytes_read, 0);

    if (ret == -1 && errno == EINTR)
      continue;
    if (ret <= 0) {
      fprintf(stderr, "[WARNING] Endpoint closed the connection unexpectedly. "
                      "Exiting...\n");
      return -1;
    }
    bytes_read += ret;
  }
  return bytes_read;
}
