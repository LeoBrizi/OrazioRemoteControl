#include "common.h"
#include "protocol.h"
#include "webcam_manager.h"
#include <stdint.h>

int main(void) {

  // LA PRIMA PARTE E' UGUALE A tester_webcam_manager.c

  int i = 0;

  int ret;
  int fd;
  int length;
  void *webcam_buffer;
  int b_before, b_after;
  char s[20];
  char c;
  int w, h;

  // apro il dispositivo come se fosse un file
  // normalmente una webcam usb e' raggiungibile con /dev/video1
  // video0 invece corrisponde alla webcam del notebook

  fd = open("/dev/video0", O_RDWR);
  PRINT_ERROR((fd < 0), errno, "Errore apertura webcam \n");

  ret = initialize_webcam(fd, &webcam_buffer, &w, &h);
  PRINT_ERROR((ret != 0), errno, "Errore inizialize_webcam \n");

  printf("\n RESET DI TUTTI I PARAMETRI \n\n");

  set_default_brightness(fd);

  set_default_contrast(fd);

  set_default_saturation(fd);

  while (1) {

    printf("digitare q per aumentare luminosita', w per diminuire \n");
    printf("digitare a per aumentare contrasto, s per diminuire \n");
    printf("digitare z per aumentare saturazione, x per diminuire \n");
    printf("digitare un altro carattere per non cambiare i parametri \n");
    printf("digitare quit per uscire\n");
    printf(":  ");

    scanf("%s", s);
    printf("\n");

    if (strcmp(s, "quit") == 0) {
      close(fd);
      printf("fine \n");
      return EXIT_SUCCESS;
    }

    c = s[0];

    switch (c) {
    case 'q':
      ret = change_brightness(fd, 10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_brightness \n");
      break;
    case 'w':
      ret = change_brightness(fd, -10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_brightness \n");
      break;
    case 'a':
      ret = change_contrast(fd, 10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_contrast \n");
      break;
    case 's':
      ret = change_contrast(fd, -10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_contrast \n");
      break;
    case 'z':
      ret = change_saturation(fd, 10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_saturation \n");
      break;
    case 'x':
      ret = change_saturation(fd, -10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_saturation \n");
      break;
    default:
      break;
    }

    printf("\n");
    ret = take_a_frame(fd, &length);
    PRINT_ERROR((ret != 0), errno, "Errore take_a_frame \n");

    // PARTE CHE VA SUL SERVER (INVIO IMMAGINE):

    printf("creazione pacchetto \n");

    PicturePacket *picture_packet =
        (PicturePacket *)malloc(sizeof(PicturePacket));
    picture_packet->header.type = PictureMsg;
    // bisogna dire che il buffer che contiene l'immagine e' mmappato nella
    // webcam quindi non va fatta la free (serve a Packet_free())
    picture_packet->mmapped_buffer = 1;
    picture_packet->length = length;
    picture_packet->height = h;
    picture_packet->width = w;
    picture_packet->picture = webcam_buffer;

    printf("pacchetto creato \n");

    printf("serializzazione pacchetto \n");

    char *serial_buffer = (char *)malloc(sizeof(PicturePacket) + length);
    int packet_len =
        Packet_serialize(serial_buffer, (PacketHeaderS *)picture_packet);

    printf("serializzazione terminata\n");

    printf("free del pacchetto serializzato\n");
    Packet_free((PacketHeaderS *)picture_packet);

    printf("valore length: %d\n", length);
    printf("valore sizeof(PicturePacket): %d\n", (int)sizeof(PicturePacket));
    printf("dimensioni pacchetto: %d\n", packet_len);

    // PARTE CHE VA SUL CLIENT (RICEZIONE IMMAGINE):

    printf("deserializzazione pacchetto\n");

    PicturePacket *packet_ricevuto = malloc(sizeof(PicturePacket));
    packet_ricevuto->header.type = PictureMsg;

    // Packet_deserialize() restituisce direttamente il pacchetto con
    // mmapped_buffer=0
    packet_ricevuto =
        (PicturePacket *)Packet_deserialize(serial_buffer, packet_len);

    printf("pacchetto deserializzato\n");

    printf("creazione file jpeg: \n");

    ret = jpeg_file_create("myjpeg.jpeg", &(packet_ricevuto->picture),
                           packet_ricevuto->length);
    PRINT_ERROR((ret != 0), errno, "Errore jpeg_file_create \n");

    printf("\n");

    Packet_free((PacketHeaderS *)packet_ricevuto);
    free(serial_buffer);
  }

  close(fd);
  printf("fine \n");

  return EXIT_SUCCESS;
}
