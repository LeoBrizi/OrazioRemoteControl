#include "log_manager.h"
#include "webcam_manager.h"
#include <time.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include "opencv2/imgcodecs.hpp"

using namespace cv;
using namespace std;

void *_loggerFn(void *args_) {
  LoggerParams *args = (LoggerParams *)args_;
  int ret;

  time_t mytime = time(NULL);
  char *time_str;

  // aprire il file
  FILE *log;
  log = fopen("log/monitor.log", "w");
  PRINT_ERROR((log == NULL), errno, "errore nell'apertura del file di log\n");
  char *socket_buffer = (char *)malloc(BUFFER_RECV_FOTO_CLIENT);
  int packet_len;
  char *cmd = (char *)"f\0";

  CommandPacket *cmd_packet = (CommandPacket *)malloc(sizeof(CommandPacket));
  ((PacketHeaderS *)cmd_packet)->type = CommandMsg;
  cmd_packet->cmd = (char *)malloc(strlen(cmd) + 1);
  memcpy(cmd_packet->cmd, cmd, strlen(cmd) + 1);

  // creazione finestra per visualizzazione immagine
  Mat image;
  Mat im_loading;

  namedWindow("Logger", WINDOW_AUTOSIZE);

  im_loading = imread("loading.jpeg", IMREAD_COLOR);
  imshow("Logger", im_loading);
  imshow("Logger", im_loading); // meglio chiamarla due volte
  waitKey(1);

  printf("[LOGGERTHREAD] sono partito e pronto a fare il log\n");
  int nome_imm = 0;

  while (*(args->runThread)) {
    memset(socket_buffer, 0, BUFFER_RECV_FOTO_CLIENT);
    packet_len = Packet_serialize(socket_buffer, (PacketHeaderS *)cmd_packet);

    ret = sem_wait(args->socket_sem);
    PRINT_ERROR((ret != 0), errno, "Errore nella sem_wait richiesta_socket\n");

    ret = invia(args->socket_desc, socket_buffer, packet_len,
                (int *)args->runThread);
    if (ret < 0) {
      *(args->runThread) = 0;
      ret = sem_post(args->socket_sem);
      PRINT_ERROR((ret != 0), errno,
                  "Errore nella sem_post richiesta_socket\n");
      break;
    }
    memset(socket_buffer, 0, BUFFER_RECV_FOTO_CLIENT);
    packet_len =
        ricevi(args->socket_desc, socket_buffer, (int *)args->runThread);
    if (packet_len == -1) {
      *(args->runThread) = 0;
      ret = sem_post(args->socket_sem);
      PRINT_ERROR((ret != 0), errno,
                  "Errore nella sem_post richiesta_socket\n");
      break;
    }
    PacketHeaderS *ack_packet = Packet_deserialize(socket_buffer, packet_len);
    Packet_free(ack_packet);

    // ricevo immagine
    memset(socket_buffer, 0, BUFFER_RECV_FOTO_CLIENT);
    packet_len =
        ricevi(args->socket_desc, socket_buffer, (int *)args->runThread);
    if (packet_len == -1) {
      *(args->runThread) = 0;
      ret = sem_post(args->socket_sem);
      PRINT_ERROR((ret != 0), errno,
                  "Errore nella sem_post richiesta_socket\n");
      break;
    }

    PacketHeaderS *picture_packet =
        Packet_deserialize(socket_buffer, packet_len);

    // ricevo odometria
    memset(socket_buffer, 0, BUFFER_RECV_FOTO_CLIENT);
    packet_len =
        ricevi(args->socket_desc, socket_buffer, (int *)args->runThread);
    if (packet_len == -1) {
      *(args->runThread) = 0;
      ret = sem_post(args->socket_sem);
      PRINT_ERROR((ret != 0), errno,
                  "Errore nella sem_post richiesta_socket\n");
      break;
    }

    PacketHeaderS *odom_packet = Packet_deserialize(socket_buffer, packet_len);
    // fine sessione critica
    ret = sem_post(args->socket_sem);
    PRINT_ERROR((ret != 0), errno, "Errore nella sem_post richiesta_socket\n");
    // prendo l'imagine e la visualizzo;

    if (!*(args->runThread)) {
      break;
    }

    time_str = ctime(&mytime);
    time_str[strlen(time_str) - 1] = '\0';
    fprintf(log, "\n");
    fprintf(log, "%s/n", time_str);
    float x = ((OdomPacket *)odom_packet)->x;
    float y = ((OdomPacket *)odom_packet)->y;
    float theta = ((OdomPacket *)odom_packet)->theta;
    fprintf(log, "ODOM: X: %f, Y: %f, theta: %f    ", x, y, theta);

    if (!image.empty()) {
      char nome[20];
      sprintf(nome, "%s%d%s", "./log/", nome_imm, ".jpeg");
      jpeg_file_create(nome, &(((PicturePacket *)picture_packet)->picture),
                       ((PicturePacket *)picture_packet)->length);
      nome_imm++;
      fprintf(log, "immagine associata: %s \n", nome);
    }

    // VISUALIZZAZIONE IMMAGINE

    Mat rawData(1, ((PicturePacket *)picture_packet)->length, CV_8UC1,
                (void *)((PicturePacket *)picture_packet)->picture);
    image = imdecode(rawData, IMREAD_COLOR);
    if (image.empty()) {
      Packet_free(odom_packet);
      Packet_free(picture_packet);
      continue;
    }

    imshow("Logger", image);
    waitKey(2);
    sleep(2);
    Packet_free(odom_packet);
    Packet_free(picture_packet);
  }

  // distruzione finestra visualizzazione immagine
  destroyWindow("Logger");
  Packet_free((PacketHeaderS *)cmd_packet);
  free(socket_buffer);
  ret = fclose(log);
  PRINT_ERROR((ret == EOF), errno, "errore nella chiusare del file di log\n");
  return 0;
}
