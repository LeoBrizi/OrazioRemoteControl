#include "webcam_manager.h"

// initialize_webcam prende come parametri il descrittore del "file" webcam e un
// puntatore al buffer che si intende usare

int initialize_webcam(int fd, void **webcam_buffer, int *width, int *height) {

  int ret;
  // quando si usa ioctl(), l'informazione su cosa deve fare si passa come
  // parametro.
  // Per esempio, in questo caso gli viene passato VIDIOC_QUERYCAP che
  // corrisponde ad una richiesta delle "capacita'"
  // RICHIESTA INFORMAZIONI ALLA WEBCAM
  // chiedo alla webcam le sue capabilities (capacita')
  // tutti i device v4l2 devono gestire almeno questa richiesta
  struct v4l2_capability cap = {};
  ret = ioctl(fd, VIDIOC_QUERYCAP, &cap);

  PRINT_ERROR((ret < 0), errno, "Errore VIDIOC_QUERYCAP\n");

  // stampo tutti i parametri richiamati

  printf("Driver Caps:\n"
         "  Driver: \"%s\"\n"
         "  Card: \"%s\"\n"
         "  Bus: \"%s\"\n"
         "  Version: %d.%d\n"
         "  Capabilities: %08x\n",
         cap.driver, cap.card, cap.bus_info, (cap.version >> 16) && 0xff,
         (cap.version >> 24) && 0xff, cap.capabilities);

  // controllo se la webcam supporta il single-planar video capture --> serve
  // per catturare un video

  ret = cap.capabilities & V4L2_CAP_VIDEO_CAPTURE;

  PRINT_ERROR(
      (ret == 0), errno,
      "Errore il device non supporta il single-planar video capture \n");

  // controllo se la webcam supporta il frame streaming

  ret = cap.capabilities & V4L2_CAP_STREAMING;

  PRINT_ERROR((ret == 0), errno,
              "Errore il device non supporta il frame streaming \n");

  // ora controllo se la webcam supporta il formato MJPEG

  int support_mjpeg = 0;

  // la struct fmtdesc conterra' una lista di formati supportati

  struct v4l2_fmtdesc fmtdesc = {0};

  // ogni volta bisogna dire a che tipo di device ci riferiamo

  fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  while (0 == ioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)) {
    if (fmtdesc.pixelformat == V4L2_PIX_FMT_MJPEG)
      support_mjpeg = 1;
    fmtdesc.index++;
  }

  ret = support_mjpeg;

  PRINT_ERROR((ret == 0), errno,
              "Errore il device non supporta il formato mjpeg \n");

  // TRASMISSIONE ALLA WEBCAM DEI PARAMETRI DESIDERATI

  // qui setto finalmente i parametri che voglio

  struct v4l2_format fmt = {0};
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.width = 752;
  fmt.fmt.pix.height = 480;
  fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;

  ret = ioctl(fd, VIDIOC_S_FMT, &fmt);

  PRINT_ERROR((ret < 0), errno, "Errore nel settaggio di Pixel Format \n");

  // RICHIESTA PARAMETRI EFFETTIVAMENTE IMPOSTATI

  struct v4l2_format fmt2 = {0};
  fmt2.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  ret = ioctl(fd, VIDIOC_G_FMT, &fmt2);

  PRINT_ERROR((ret < 0), errno, "Errore nella richiesta di Pixel Format \n");

  // assegno alle variabili passate come parametro i valori effettivamente
  // impostati dalla webcam

  *width = fmt2.fmt.pix.width;
  *height = fmt2.fmt.pix.height;

  // TRASMINSSIONE INFORMAZIONI RIGUARDANTI I/IL BUFFER DA UTILIZZARE PER
  // COMUNICARE

  // ora invio i dati riguardanti i buffers che uso per comunicare con la webcam

  struct v4l2_requestbuffers bufrequest = {0};
  bufrequest.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufrequest.memory = V4L2_MEMORY_MMAP;

  // il .count serve a dire quanti buffers si vogliono usare
  bufrequest.count = 1;

  ret = ioctl(fd, VIDIOC_REQBUFS, &bufrequest);

  PRINT_ERROR((ret < 0), errno, "Errore VIDIOC_REQBUFS \n");

  // ALLOCAZIONE DEI/DEL BUFFER DA USARE

  // ora chiedo di che grandezza devo allocare i buffers
  struct v4l2_buffer bufferinfo;

  // non sono l'unico a scrivere su questa struttura quindi mi assicuro che
  // bufferinfo non contenga altri dati
  memset(&bufferinfo, 0, sizeof(bufferinfo));

  bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufferinfo.memory = V4L2_MEMORY_MMAP;
  bufferinfo.index = 0;

  ret = ioctl(fd, VIDIOC_QUERYBUF, &bufferinfo);

  PRINT_ERROR((ret < 0), errno, "Errore VIDIOC_QUERYBUF \n");

  // ora che so quanta memoria mi serve, mappo il buffer
  // bufferinfo.length e' stato riempito da VIDIOC_QUERYBUF

  *webcam_buffer = mmap(NULL, bufferinfo.length, PROT_READ | PROT_WRITE,
                        MAP_SHARED, fd, bufferinfo.m.offset);

  PRINT_ERROR((*webcam_buffer == MAP_FAILED), errno, "Errore in mmap \n");

  // pulisco il buffer per non avere rumore nel frame che salvo
  memset(*webcam_buffer, 0, bufferinfo.length);

  printf("webcam inizializzata con successo \n");

  return 0;
}

// take_a_frame prende come parametri il descrittore al "file" webcam e un
// puntatore ad int in cui verra' scritta la lunghezza del buffer una volta
// riempito
// una volta che la funzione ritorna si avra' il frame pronto nel webcam_buffer

int take_a_frame(int fd, int *length) {

  int ret;
  // PREPARAZIONE INFORMAZIONI DEL BUFFER DA INVIARE

  struct v4l2_buffer bufferinfo;

  // non sono l'unico a scrivere su questa struttura quindi mi assicuro che
  // bufferinfo non contenga altri dati
  memset(&bufferinfo, 0, sizeof(bufferinfo));

  bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufferinfo.memory = V4L2_MEMORY_MMAP;
  bufferinfo.index = 0;

  // ATTIVAZIONE STREAMING

  // attivo lo streaming
  int type = bufferinfo.type;
  ret = ioctl(fd, VIDIOC_STREAMON, &type);

  PRINT_ERROR((ret < 0), errno, "VIDIOC_STREAMON \n");

  // TRASFERIMENTO BUFFER NEL DEVICE

  // trasferisco il mio buffer nel device e aspetto che ci scriva cose dentro
  ret = ioctl(fd, VIDIOC_QBUF, &bufferinfo);

  PRINT_ERROR((ret < 0), errno, "VIDIOC_QBUF \n");

  // RECUPERO DEL BUFFER RIEMPITO DAL DEVICE

  // riprendo dal device il mio buffer
  ret = ioctl(fd, VIDIOC_DQBUF, &bufferinfo);

  PRINT_ERROR((ret < 0), errno, "VIDIOC_DQBUF \n");

  // DISATTIVAZIONE STREAMING

  // disattivo lo streaming

  ret = ioctl(fd, VIDIOC_STREAMOFF, &type);

  PRINT_ERROR((ret < 0), errno, "VIDIOC_STREAMOFF \n");

  *length = bufferinfo.length;

  printf("frame pronto \n");

  return 0;
}

// jpeg_file_create prende come parametri il puntatore a webcam_buffer e length,
// entrambi usati dalla write per scrivere nel file

int jpeg_file_create(char nomejpeg[], void **webcam_buffer, int length) {
  // SALVATAGGIO IMMAGINE IN FILE JPEG

  // salvo l'immagine acquisita su un file jpeg

  int jpgfile;

  jpgfile = open(nomejpeg, O_WRONLY | O_CREAT, 0660);

  PRINT_ERROR((jpgfile < 0), errno, "Errore nella creazione del file jpeg \n");

  write(jpgfile, *webcam_buffer, length);

  close(jpgfile);

  return 0;
}

// FUNZIONE change_brightness()
// prende in input il descrittore della webcam, il delta da aggiungere (sia
// positivo sia negativo)
// un puntatore ad int per il valore iniziale e un puntatore ad int per il
// valore effettivamente impostato

// ISTRUZIONI: se valore iniziale e valore impostato risultano uguali vuol dire
// che ha raggiunto il limite di luminosita'
// restituisce 0 in caso di successo, 1 in caso non ci sono stati errori ma il
// device non supporta V4L2_CID_BRIGHTNESS

int change_brightness(int fd, int delta, int *previous_value,
                      int *changed_value) {

  int ret;

  struct v4l2_queryctrl queryctrl;
  struct v4l2_control control;

  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_BRIGHTNESS;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {

    // se errno != da EINVAL c'e' stato un errore nella VIDIOC_QUERYCTRL
    PRINT_ERROR((errno != EINVAL), errno, "VIDIOC_QUERYCTRL \n");

    // altrimenti significa che V4L2_CID_BRIGHTNESS non è supportato dal device
    // questo non fa terminare il programma
    printf("V4L2_CID_BRIGHTNESS is not supported \n");

    return 1;

  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf("V4L2_CID_BRIGHTNESS is not supported \n");

    return 1;

  } else {

    // se arriva in questo punto, e' andato tutto bene quindi  può procedere con
    // il settaggio del parametro

    memset(&control, 0, sizeof(control));
    control.id = V4L2_CID_BRIGHTNESS;

    // questo serve per visualizzare il valore attuale
    ret = ioctl(fd, VIDIOC_G_CTRL, &control);
    PRINT_ERROR((ret != 0 && errno != EINVAL), errno, "VIDIOC_G_CTRL \n");

    // in control.value e' stato inserito dal device il valore attuale
    // questa assegnazione serve per far sapere al chiamante il valore attuale

    printf("LUMINOSITA':  valore INIZIALE = %d \n", control.value);
    *previous_value = control.value;

    // controlla se si cerca di superare i limiti consentiti dal device
    // queryctrl.minimum e queryctrl.maximum sono i limiti del device

    if (delta + *previous_value < queryctrl.minimum)
      control.value = queryctrl.minimum;

    else if (delta + *previous_value > queryctrl.maximum)
      control.value = queryctrl.maximum;

    else
      control.value = delta + *previous_value;
    printf("LUMINOSITA':  valore IMPOSTATO = %d \n", control.value);

    // questa assegnazione serve per far sapere al chiamante il valore impostato
    *changed_value = control.value;

    ret = ioctl(fd, VIDIOC_S_CTRL, &control);
    PRINT_ERROR((ret == -1), errno, "VIDIOC_S_CTRL \n");
  }

  return 0;
}

// FUNZIONE change_contrast()
// stessa cosa di change_brightness

int change_contrast(int fd, int delta, int *previous_value,
                    int *changed_value) {

  int ret;

  struct v4l2_queryctrl queryctrl;
  struct v4l2_control control;

  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_CONTRAST;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {

    // se errno != da EINVAL c'e' stato un errore nella VIDIOC_QUERYCTRL
    PRINT_ERROR((errno != EINVAL), errno, "VIDIOC_QUERYCTRL \n");

    // altrimenti significa che V4L2_CID_CONTRAST non è supportato dal device
    // questo non fa terminare il programma
    printf("V4L2_CID_CONTRAST is not supported \n");

    return 1;

  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf("V4L2_CID_CONTRAST is not supported \n");

    return 1;

  } else {

    // se arriva in questo punto, e' andato tutto bene quindi  può procedere con
    // il settaggio del parametro

    memset(&control, 0, sizeof(control));
    control.id = V4L2_CID_CONTRAST;

    // questo serve per visualizzare il valore attuale
    ret = ioctl(fd, VIDIOC_G_CTRL, &control);
    PRINT_ERROR((ret != 0 && errno != EINVAL), errno, "VIDIOC_G_CTRL \n");

    // in control.value e' stato inserito dal device il valore attuale
    // questa assegnazione serve per far sapere al chiamante il valore attuale

    printf("CONTRASTO:  valore INIZIALE = %d \n", control.value); // DEBUG
    *previous_value = control.value;

    // controlla se si cerca di superare i limiti consentiti dal device
    // queryctrl.minimum e queryctrl.maximum sono i limiti del device

    if (delta + *previous_value < queryctrl.minimum)
      control.value = queryctrl.minimum;

    else if (delta + *previous_value > queryctrl.maximum)
      control.value = queryctrl.maximum;

    else
      control.value = delta + *previous_value;
    printf("CONTRASTO:  valore IMPOSTATO = %d \n", control.value); // DEBUG

    // questa assegnazione serve per far sapere al chiamante il valore impostato
    *changed_value = control.value;

    ret = ioctl(fd, VIDIOC_S_CTRL, &control);
    PRINT_ERROR((ret == -1), errno, "VIDIOC_S_CTRL \n");
  }

  return 0;
}

// FUNZIONE change_saturation()
// stessa cosa di change_brightness

int change_saturation(int fd, int delta, int *previous_value,
                      int *changed_value) {

  int ret;

  struct v4l2_queryctrl queryctrl;
  struct v4l2_control control;

  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_SATURATION;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {

    // se errno != da EINVAL c'e' stato un errore nella VIDIOC_QUERYCTRL
    PRINT_ERROR((errno != EINVAL), errno, "VIDIOC_QUERYCTRL \n");

    // altrimenti significa che V4L2_CID_SATURATION non è supportato dal device
    // questo non fa terminare il programma
    printf("V4L2_CID_SATURATION is not supported \n");

    return 1;

  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf("V4L2_CID_SATURATION is not supported \n");

    return 1;

  } else {

    // se arriva in questo punto, e' andato tutto bene quindi  può procedere con
    // il settaggio del parametro

    memset(&control, 0, sizeof(control));
    control.id = V4L2_CID_SATURATION;

    // questo serve per visualizzare il valore attuale
    ret = ioctl(fd, VIDIOC_G_CTRL, &control);
    PRINT_ERROR((ret != 0 && errno != EINVAL), errno, "VIDIOC_G_CTRL \n");

    // in control.value e' stato inserito dal device il valore attuale
    // questa assegnazione serve per far sapere al chiamante il valore attuale

    printf("SATURAZIONE:  valore INIZIALE = %d \n", control.value); // DEBUG
    *previous_value = control.value;

    // controlla se si cerca di superare i limiti consentiti dal device
    // queryctrl.minimum e queryctrl.maximum sono i limiti del device
    // DEBUG
    if (delta + *previous_value < queryctrl.minimum)
      control.value = queryctrl.minimum;

    else if (delta + *previous_value > queryctrl.maximum)
      control.value = queryctrl.maximum;

    else
      control.value = delta + *previous_value;
    printf("SATURAZIONE:  valore IMPOSTATO = %d \n", control.value); // DEBUG

    // questa assegnazione serve per far sapere al chiamante il valore impostato
    *changed_value = control.value;

    ret = ioctl(fd, VIDIOC_S_CTRL, &control);
    PRINT_ERROR((ret == -1), errno, "VIDIOC_S_CTRL \n");
  }

  return 0;
}

// FUNZIONI CHE SERVONO A RESETTARE I PARAMETRI

int set_default_brightness(int fd) {

  int ret;

  struct v4l2_queryctrl queryctrl;
  struct v4l2_control control;

  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_BRIGHTNESS;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {

    // se errno != da EINVAL c'e' stato un errore nella VIDIOC_QUERYCTRL
    PRINT_ERROR((errno != EINVAL), errno, "VIDIOC_QUERYCTRL \n");

    // altrimenti significa che V4L2_CID_SATURATION non è supportato dal device
    // questo non fa terminare il programma
    printf("V4L2_CID_BRIGHTNESS is not supported \n");

    return 1;

  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf("V4L2_CID_BRIGHTNESS is not supported \n");

    return 1;

  } else {

    // se arriva in questo punto, e' andato tutto bene quindi  può procedere con
    // il settaggio del parametro

    memset(&control, 0, sizeof(control));
    control.id = V4L2_CID_BRIGHTNESS;

    // questo serve per visualizzare il valore attuale
    ret = ioctl(fd, VIDIOC_G_CTRL, &control);
    PRINT_ERROR((ret != 0 && errno != EINVAL), errno, "VIDIOC_G_CTRL \n");

    control.value = queryctrl.default_value;

    ret = ioctl(fd, VIDIOC_S_CTRL, &control);
    PRINT_ERROR((ret == -1), errno, "VIDIOC_S_CTRL \n");
  }

  return 0;
}

int set_default_contrast(int fd) {

  int ret;

  struct v4l2_queryctrl queryctrl;
  struct v4l2_control control;

  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_CONTRAST;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {

    // se errno != da EINVAL c'e' stato un errore nella VIDIOC_QUERYCTRL
    PRINT_ERROR((errno != EINVAL), errno, "VIDIOC_QUERYCTRL \n");

    // altrimenti significa che V4L2_CID_SATURATION non è supportato dal device
    // questo non fa terminare il programma
    printf("V4L2_CID_CONTRAST is not supported \n");

    return 1;

  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf("V4L2_CID_CONTRAST is not supported \n");

    return 1;

  } else {

    // se arriva in questo punto, e' andato tutto bene quindi  può procedere con
    // il settaggio del parametro

    memset(&control, 0, sizeof(control));
    control.id = V4L2_CID_CONTRAST;

    // questo serve per visualizzare il valore attuale
    ret = ioctl(fd, VIDIOC_G_CTRL, &control);
    PRINT_ERROR((ret != 0 && errno != EINVAL), errno, "VIDIOC_G_CTRL \n");

    control.value = queryctrl.default_value;

    ret = ioctl(fd, VIDIOC_S_CTRL, &control);
    PRINT_ERROR((ret == -1), errno, "VIDIOC_S_CTRL \n");
  }

  return 0;
}

int set_default_saturation(int fd) {

  int ret;

  struct v4l2_queryctrl queryctrl;
  struct v4l2_control control;

  memset(&queryctrl, 0, sizeof(queryctrl));
  queryctrl.id = V4L2_CID_SATURATION;

  if (-1 == ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {

    // se errno != da EINVAL c'e' stato un errore nella VIDIOC_QUERYCTRL
    PRINT_ERROR((errno != EINVAL), errno, "VIDIOC_QUERYCTRL \n");

    // altrimenti significa che V4L2_CID_SATURATION non è supportato dal device
    // questo non fa terminare il programma
    printf("V4L2_CID_SATURATION is not supported \n");

    return 1;

  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    printf("V4L2_CID_SATURATION is not supported \n");

    return 1;

  } else {

    // se arriva in questo punto, e' andato tutto bene quindi  può procedere con
    // il settaggio del parametro

    memset(&control, 0, sizeof(control));
    control.id = V4L2_CID_SATURATION;

    // questo serve per visualizzare il valore attuale
    ret = ioctl(fd, VIDIOC_G_CTRL, &control);
    PRINT_ERROR((ret != 0 && errno != EINVAL), errno, "VIDIOC_G_CTRL \n");

    control.value = queryctrl.default_value;

    ret = ioctl(fd, VIDIOC_S_CTRL, &control);
    PRINT_ERROR((ret == -1), errno, "VIDIOC_S_CTRL \n");
  }

  return 0;
}
