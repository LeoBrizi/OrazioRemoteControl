#pragma once
#include "common.h"
#include "protocol.h"

// non bloccante
// invia sul file descriptor un buffer di lunghezza msg_len,
// la variabile running server per capire se l'ivio deve terminare all'arrivo di
// un segnale o no
// la funzione ritorna il numero di byte inviati in caso di successo, se il
// filedescriptor viene chiuso -1
int invia(int fd, char *buffer, int msg_len, int *running);

// bloccante
// ricevi dal file descriptor un pacchetto che viene memorizzato in buffer
// running serve per sapere se terminare o no all'arrivo di un segnale
// in caso di successo la funzione ritorna i byte ricevuti
// se il filedescriptor viene chiuso ritorna -1
int ricevi(int fd, char *buffer, int *running);
