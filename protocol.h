#pragma once
#include "common.h"

typedef enum {
  PictureMsg = 0x1,
  CommandMsg = 0x2,
  OdomMsg = 0x3,
  AckMsg = 0x4
} Type;

typedef struct {
  Type type;
  int size;
} PacketHeaderS;

typedef struct {
  PacketHeaderS header;
  int mmapped_buffer; // serve a Packet_free(): la free del buffer mmappato
                      // nella webcam fallisce
  // mmapped_buffer=1 se il buffer e' quello mmappato (cioe' e' nel processo che
  // usa la webcam)
  // mmapped_buffer=0 se il pacchetto e' stato deserializzato (nel processo
  // client)
  // Packet_deserialize() restituisce direttamente il pacchetto con
  // mmapped_buffer=0
  int length;
  int height;
  int width;
  void *picture;
} PicturePacket;

typedef struct {
  PacketHeaderS header;
  char *cmd;
} CommandPacket; //

typedef struct {
  PacketHeaderS header;
  float x;
  float y;
  float theta;
} OdomPacket;

// serializza il pacchetto nel buffer vuoto preallocato
int Packet_serialize(char *dest, PacketHeaderS *h);

// returns a newly allocated packet read from the buffer
PacketHeaderS *Packet_deserialize(const char *buffer, int size);

// deletes a packet, freeing memory
void Packet_free(PacketHeaderS *h);
