#include "orazio_thread.h"

void *_orazioFn(void *args_) {
  OrazioParams *args = (OrazioParams *)args_;

  int old_cmd = *(args->cmd);
  int ret;

  while (*(args->runThread)) {
    OrazioClient_sync(args->client, 10);
    // OrazioClient_readConfiguration(args->client, 10);
    float tv = 0.0f;
    float rv = 0.0f;
    int new_cmd = *(args->cmd);

    if (new_cmd != old_cmd && new_cmd != 6)
      old_cmd = new_cmd;

    switch (new_cmd) {
    case 1:
      tv = 0.1f;
      rv = 0.0f;
      break;
    case 2:
      tv = -0.1f;
      rv = 0.0f;
      break;
    case 3:
      tv = 0.0f;
      rv = -0.4f;
      break;
    case 4:
      tv = 0.0f;
      rv = 0.4f;
      break;
    case 5:
      tv = 0.0f;
      rv = 0.0f;
      break;
    case 6:
      OrazioClient_sync(args->client, 10);
      OrazioClient_getOdometryPosition(args->client, args->odom_x, args->odom_y,
                                       args->odom_theta);
      new_cmd = old_cmd;
      *(args->cmd) = old_cmd;
      ret = sem_post(args->odom_sem);
      PRINT_ERROR((ret != 0), errno, "Errore nella sem_post odom_sem\n");

      break;
    default:
      continue;
    }

    DifferentialDriveControlPacket base_control = {
        {.type = DIFFERENTIAL_DRIVE_CONTROL_PACKET_ID,
         .size = sizeof(DifferentialDriveControlPacket),
         .seq = 0},
        .translational_velocity = tv,
        .rotational_velocity = rv};
    OrazioClient_sendPacket(args->client, (PacketHeader *)&base_control, 0);
  }
  DifferentialDriveControlPacket base_control = {
      {.type = DIFFERENTIAL_DRIVE_CONTROL_PACKET_ID,
       .size = sizeof(DifferentialDriveControlPacket),
       .seq = 0},
      .translational_velocity = 0.0f,
      .rotational_velocity = 0.0f};
  OrazioClient_sendPacket(args->client, (PacketHeader *)&base_control, 0);
  return 0;
}
