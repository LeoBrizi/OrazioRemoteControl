// per compilare: gcc tester_webcam_manager.c webcam_manager.c -o tester
// consiglio: eseguire una volta, aprire il file jpeg creato e rieseguire altre
// volte
// l'immagine aperta si aggiorna da sola
#include "common.h"
#include "webcam_manager.h"

int main(void) {

  int i = 0;

  int ret;
  int fd;
  int length;
  void *webcam_buffer;
  int b_before, b_after;
  char s[20];
  char c;
  int w, h;

  // apro il dispositivo come se fosse un file
  // normalmente una webcam usb e' raggiungibile con /dev/video1
  // video0 invece corrisponde alla webcam del notebook

  fd = open("/dev/video0", O_RDWR);
  PRINT_ERROR((fd < 0), errno, "Errore apertura webcam \n");

  ret = initialize_webcam(fd, &webcam_buffer, &w, &h);
  PRINT_ERROR((ret != 0), errno, "Errore inizialize_webcam \n");

  printf("\n RESET DI TUTTI I PARAMETRI \n\n");

  set_default_brightness(fd);

  set_default_contrast(fd);

  set_default_saturation(fd);

  while (1) {

    printf("digitare q per aumentare luminosita', w per diminuire \n");
    printf("digitare a per aumentare contrasto, s per diminuire \n");
    printf("digitare z per aumentare saturazione, x per diminuire \n");
    printf("digitare un altro carattere per non cambiare i parametri \n");
    printf("digitare quit per uscire\n");
    printf(":  ");

    scanf("%s", s);
    printf("\n");

    if (strcmp(s, "quit") == 0) {
      close(fd);
      printf("fine \n");
      return EXIT_SUCCESS;
    }

    c = s[0];

    switch (c) {
    case 'q':
      ret = change_brightness(fd, 10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_brightness \n");
      break;
    case 'w':
      ret = change_brightness(fd, -10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_brightness \n");
      break;
    case 'a':
      ret = change_contrast(fd, 10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_contrast \n");
      break;
    case 's':
      ret = change_contrast(fd, -10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_contrast \n");
      break;
    case 'z':
      ret = change_saturation(fd, 10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_saturation \n");
      break;
    case 'x':
      ret = change_saturation(fd, -10, &b_before, &b_after);
      PRINT_ERROR((ret != 0 && ret != 1), errno, "Errore change_saturation \n");
      break;
    default:
      break;
    }

    printf("\n");
    ret = take_a_frame(fd, &length);
    PRINT_ERROR((ret != 0), errno, "Errore take_a_frame \n");

    ret = jpeg_file_create("myjpeg.jpeg", &webcam_buffer, length);
    PRINT_ERROR((ret != 0), errno, "Errore jpeg_file_create \n");
    printf("\n");
  }

  close(fd);
  printf("fine \n");

  return EXIT_SUCCESS;
}
