#pragma once
#include "Orazio/orazio_client.h"
#include "common.h"
#include <semaphore.h>

// parametri che servono al thread che si occupa di gestire la comunicazione dei
// comandi al firmware
typedef struct {
  struct OrazioClient *client;
  volatile int *runThread;
  int *cmd;
  float *odom_x;
  float *odom_y;
  float *odom_theta;
  sem_t *odom_sem;
} OrazioParams;

// funzione eseguita dal thread
void *_orazioFn(void *args_);