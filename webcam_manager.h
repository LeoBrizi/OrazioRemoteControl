#include "common.h" //PRINT_ERROR
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

// extern void* webcam_buffer;

// initialize_webcam prende come parametri il descrittore del "file" webcam, un
// puntatore al buffer che si intende usare
// e i puntatori a width e height per sapere i valori effettivamente impostati
// dalla webcam
int initialize_webcam(int fd, void **webcam_buffer, int *width, int *height);

// take_a_frame prende come parametri il descrittore al "file" webcam e un
// puntatore ad int in cui verra' scritta la lunghezza del buffer una volta
// riempito
// una volta che la funzione ritorna si avra' il frame pronto nel webcam_buffer
int take_a_frame(int fd, int *length);

// jpeg_file_create prende come parametri il puntatore a webcam_buffer e length,
// entrambi usati per scrivere nel file
int jpeg_file_create(char nomejpeg[], void **webcam_buffer, int length);

// FUNZIONE change_brightness()
// prende in input il descrittore della webcam, il delta da aggiungere (sia
// positivo sia negativo)
// un puntatore ad int per il valore iniziale e un puntatore ad int per il
// valore effettivamente impostato

// ISTRUZIONI: se valore iniziale e valore impostato risultano uguali vuol dire
// che ha raggiunto il limite di luminosita'
// restituisce 0 in caso di successo, 1 in caso non ci sono stati errori ma il
// device non supporta V4L2_CID_BRIGHTNESS
int change_brightness(int fd, int delta, int *previous_value,
                      int *changed_value);

// FUNZIONE change_contrast()
// stessa cosa di change_brightness
int change_contrast(int fd, int delta, int *previous_value, int *changed_value);

// FUNZIONE change_saturation()
// stessa cosa di change_brightness
int change_saturation(int fd, int delta, int *previous_value,
                      int *changed_value);

// FUNZIONI CHE SERVONO A RESETTARE I PARAMETRI
// prende in input il descrittore della webcam
// setta automaticamente il parametro al valore di default
// restituisce 0 in caso di successo, 1 in caso non ci sono stati errori ma il
// device non supporta V4L2_CID_...

int set_default_brightness(int fd);

int set_default_contrast(int fd);

int set_default_saturation(int fd);
